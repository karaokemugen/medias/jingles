# Karaoke Mugen Jingles repository

This is a simple repository for Karaoke Mugen Jingles

Jingles are placed inbetween songs when running a session with the Karaoke Mugen application.

You can [learn more about Karaoke Mugen on its website](http://mugen.karaokes.moe)

Please only submit eyecatches and jingles you are happy with and can be considered finalized.
